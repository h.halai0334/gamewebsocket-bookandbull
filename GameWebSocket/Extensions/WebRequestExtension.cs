﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace GameWebSocket.Extensions
{
    public static class WebRequestExtension
    {
        public static bool ReadCookies(this WebRequest webRequest, string fileName)
        {
            if (!File.Exists(fileName))
            {
                return false;
            }
            HttpWebRequest httpRequest = webRequest as HttpWebRequest;
            if (httpRequest == null)
            {
                return false;
            }

            if (httpRequest.CookieContainer == null)
            {
                httpRequest.CookieContainer = new CookieContainer();
            }
            var data = File.ReadAllText(fileName);
            var list = JsonConvert.DeserializeObject<List<CookieWrapper>>(data);
            foreach (var cookie in list)
            {
                var addCookie = new Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain);
                httpRequest.CookieContainer.Add(addCookie);
            }
            return true;
        }
    }
}
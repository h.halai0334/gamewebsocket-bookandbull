﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;

namespace GameWebSocket.Extensions
{
    public class CookieWrapper
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Domain { get; set; }
        public virtual string Path { get; set; }
        public virtual bool Secure { get; set; }
        public bool IsHttpOnly { get; set; }
        public DateTime? Expiry { get; set; }
    }
    public static class WebDriverExtensions
    {
        public static void SaveCookies(this IWebDriver driver, string fileName)
        {
            var cookies = driver.Manage().Cookies;
            var list = new List<CookieWrapper>();
            foreach (var cookie in cookies.AllCookies)
            {
                var wrapper = new CookieWrapper();
                list.Add(new CookieWrapper()
                {
                    Domain = cookie.Domain,
                    Expiry = cookie.Expiry,
                    Name = cookie.Name,
                    Path = cookie.Path,
                    Secure = cookie.Secure,
                    Value = cookie.Value,
                    IsHttpOnly = cookie.IsHttpOnly
                });
            }

            var json = JsonConvert.SerializeObject(list);
            File.WriteAllText(fileName, json);
        }

        public static void ReadCookies(this IWebDriver driver, string fileName)
        {
            if (!File.Exists(fileName))
            {
                return;
            }
            var cookies = driver.Manage().Cookies;
            cookies.DeleteAllCookies();
            var data = File.ReadAllText(fileName);
            var list = JsonConvert.DeserializeObject<List<CookieWrapper>>(data);
            foreach (var cookie in list)
            {
                var addCookie = new Cookie(cookie.Name, cookie.Value,cookie.Domain, cookie.Path, cookie.Expiry);
                cookies.AddCookie(addCookie);
            }
        }
        public static IWebElement FindElementBy(this IWebDriver driver, By @by)
        {
            try
            {
                return driver.FindElement(by);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public static IWebElement SetAttribute(this IWebElement element, string name, string value)
        {
            var driver = ((IWrapsDriver)element).WrappedDriver;
            var jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, name, value);

            return element;
        }
    }
}
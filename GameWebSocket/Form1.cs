﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using GameWebSocket.Extensions;
using HtmlAgilityPack;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WebSocketSharp;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace GameWebSocket
{
    public partial class Form1 : Form
    {
        private bool _isLogin = false;
        private string _reconnectToken = "";
        private readonly string _fileName = "cookies.json";
        private WebSocket _ws;
        int _counter;
        string _url = "wss://clients-rghr.igplatform.net/ws";
        private int _gambleBet = 10;
        private int _cId = 2;
        public Form1()
        {
            int lastWinAmount;
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            btnLadder.Enabled = btnSpin.Enabled = false;
            _counter = 5;
            _ws = new WebSocket(_url);
            _ws.OnMessage += (sender, e) => {
                
                string data = e.Data;
                Log(data);
                if (data.Contains("\"name\":\"Authenticate\""))
                {
                    Log("Authenticated");
                    var response = JsonConvert.DeserializeObject<dynamic>(data);
                    if (response?.body?.balance != null)
                    {
                        SetBalance((int)response.body.balance);
                        if (response.body.recconectToken != null)
                        {
                            _reconnectToken = (string) response.body.reconnectToken;
                        }
                    }
                    OpenGame();
                    Invoke(new Action(() =>
                    {
                        btnSpin.Enabled = true;
                    }));
                }
                if (data.Contains("\"name\":\"OpenGame\""))
                {
                    Log("Game Opened");
                    Invoke(new Action(() =>
                    {
                        btnSpin.Enabled = true;
                    }));
                }
                if (data.Contains("\"name\":\"CloseGameUpdate\""))
                {
                    Log("Game Ended");
                    Invoke(new Action(() =>
                    {
                        MessageBox.Show(@"Game is Closed by Server. Please Connect again");
                    }));
                }
                if (data.Contains("\"name\":\"GameEvent\""))
                {
                    if (!string.IsNullOrWhiteSpace(txtDId.Text))
                    {
                        Invoke(new Action(() =>
                        {
                            btnSpin.Enabled = true;
                        }));
                        var response = JsonConvert.DeserializeObject<dynamic>(data);
                        string send = "{\"header\":{\"mId\": " + response.header.mId + ",\"cId\":" + response.header.cId +
                                      ",\"name\":\"GameEvent\",\"code\":1,\"dType\":2,\"dId\":" + txtDId.Text + "}}";
                        Send(send);
                    }
                }
                if (data.Contains("\"name\":\"Ping\""))
                {
                    //{"header":{"mId":4,"cId":29646137,"name":"Ping","code":1,"dType":2,"dId":13901054}}	
                    if (!string.IsNullOrWhiteSpace(txtDId.Text))
                    {
                        var response = JsonConvert.DeserializeObject<dynamic>(data);
                        string send = "{\"header\":{\"mId\": " + response.header.mId + ",\"cId\":" + response.header.cId +
                                      ",\"name\":\"Ping\",\"code\":1,\"dType\":2,\"dId\":" + txtDId.Text + "}}";
                        Send(send);
                    }
                }
                if (data.Contains("\"event\":\"SPIN_RESULT\""))
                {
                    Invoke(new Action(() =>
                    {
                        btnSpin.Enabled = true;
                    }));
                    var response = JsonConvert.DeserializeObject<dynamic>(data);
                    var balance = (int)response.body.balance;
                    var isGambleAllowed = (bool) response.body.data.GAMBLE_LADDER.gamblingAllowed;
                    if (response.body.data.GAMBLE_CARD_CS.maxGambleBet != null)
                    {
                        _gambleBet = (int)response.body.data.GAMBLE_CARD_CS.maxGambleBet;
                    }
                    Invoke(new Action(() => { btnLadderHalf.Enabled = _gambleBet >= 100; }));
                    lastWinAmount = (int) response.body.data.SLOT.winAmount;
                    SetLastWin(lastWinAmount);
                    SetBet(_gambleBet);
                    SetBalance(balance);
                    SetLadderStatus(isGambleAllowed);
                }
                if (data.Contains("\"event\":\"GAMBLE_RESULT\""))
                {
                    Invoke(new Action(() =>
                    {
                        btnSpin.Enabled = true;
                    }));
                    var response = JsonConvert.DeserializeObject<dynamic>(data);
                    var balance = (int)response.body.balance;
                    lastWinAmount = (int) response.body.data.GAMBLE_LADDER.gambleWin;
                    if (response.body.data.GAMBLE_CARD_CS.maxGambleBet != null)
                    {
                        _gambleBet = (int)response.body.data.GAMBLE_CARD_CS.maxGambleBet;
                    }
                    else
                    {
                        _gambleBet = lastWinAmount;
                    }
                    Invoke(new Action(() => { btnLadderHalf.Enabled = _gambleBet >= 100; }));
                    
                    SetLastWin(lastWinAmount);
                    SetBet(_gambleBet);
                    SetBalance(balance);
                    SetLadderStatus((bool)response.body.data.GAMBLE_LADDER.gamblingAllowed);
                }

                if (data.Contains("\"event\":\"OPEN_GAME\""))
                {
                    Log("Open Game Event Received");
                    var response = JsonConvert.DeserializeObject<dynamic>(data);
                    InvokeIt(() =>
                    {
                        txtDId.Text = response.header.dId.ToString();
                    });
                    var balance = (int)response.body.balance;
                    SetBalance(balance);
                }
            };
            _ws.OnOpen += (sender, e) => { Log("Connected...");
                Authenticate(); 
            };
            _ws.OnError += (sender, e) => { Log("Error : " + e.Message); };
            _ws.OnClose += (sender, e) =>
            {
                Log("Connection Closed");
                SetBalance(0);
                SetBet(0);
                SetLastWin(0);
                Invoke(new Action(() =>
                {
                    btnLadder.Enabled = false;
                    btnSpin.Enabled = false;
                    if (!string.IsNullOrWhiteSpace(e.Reason))
                    {
                        Log("Closed : " + e.Reason);
                        MessageBox.Show(@"Closed: " + e.Reason);
                    }
                }));
            };

        }

        private void SetLadderStatus(bool isEnabled)
        {
            Invoke(new Action(() =>
            {
                btnLadder.Enabled = isEnabled;
            }));
        }
        private void SetBalance(int balance)
        {
            Invoke(new Action(() =>
            {
                lblBalance.Text = "Balance: " + balance;
            }));
        }
        
        private void SetBet(int bet)
        {
            Invoke(new Action(() =>
            {
                lblBet.Text = "Bet: " + bet;
            }));
        }
        private void SetLastWin(int win)
        {
            Invoke(new Action(() =>
            {
                lblWinAmount.Text = "Last Win Amount: " + win;
            }));
        }
        private void Log(string data, bool isSend = false)
        {
            
            Invoke(new Action(() =>
            {
                if (isSend)
                {
                    txtSend.AppendText(data + "\n");
                    txtSend.SelectionStart = txtSend.Text.Length;
                    txtSend.ScrollToCaret();
                }
                else
                {
                    txtRecieve.AppendText(data + "\n");
                    txtRecieve.SelectionStart = txtRecieve.Text.Length;
                    txtRecieve.ScrollToCaret();
                }
            }));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnLadder.Enabled = false;
            btnLadderHalf.Enabled = false;
            lblBalance.Text = "Balance: 0";
            _ws.Connect();
        }

        private void WaitNSeconds(int segundos)
        {
            if (segundos < 1) return;
            DateTime desired = DateTime.Now.AddSeconds(segundos);
            while (DateTime.Now < desired)
            {
                Application.DoEvents();
            }
        }

        private void Initilaize()
        {
            
            // WaitNSeconds(10);
            // ws.Send(
            //     "{\"header\":{\"mId\":2,\"cId\":2,\"name\":\"OpenGame\",\"code\":0,\"dType\":1,\"dId\":0},\"body\":{\"gameCode\":\"GAM_BOB\",\"token\":\"\"}}");
            // WaitNSeconds(1);
            // ws.Send(
            //     "{\"header\":{\"mId\":3,\"cId\":20751812,\"name\":\"GameEvent\",\"code\":1,\"dType\":2,\"dId\":13722444}}");
            // WaitNSeconds(1);
            // ws.Send(
            //     "{\"header\":{\"mId\":4,\"cId\":20751998,\"name\":\"Ping\",\"code\":1,\"dType\":2,\"dId\":13722444}}");
            //
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var data = "{\"header\":{\"mId\":" + _counter +
                       ",\"cId\":20751998,\"name\":\"Ping\",\"code\":1,\"dType\":2,\"dId\":" + txtDId.Text + "}}";
            Send(data);
            _counter++;
        }

        private void Authenticate()
        {
            //{"header":{"mId":1,"cId":1,"name":"Authenticate","code":0,"dType":1,"dId":0},"body":{"username":"","token":"8db02f2f-b817-4e8e-bf5c-07a9ee1c197c","languageCode":"ENG","cageCode":"VAV01","skinCode":"","version":"o1.114.1","clientType":3,"playMode":1,"reconnect":false}}	
            InvokeIt(() =>
            {
                var userName = cbxAuthenticate.Checked ? txtToken.Text : "";
                if (string.IsNullOrWhiteSpace(userName))
                {
                    Send(
                        "{\"header\":{\"mId\":1,\"cId\":1,\"name\":\"Authenticate\",\"code\":0,\"dType\":1,\"dId\":0},\"body\":{\"username\":\"\",\"token\":\"token\",\"languageCode\":\"RUS\",\"cageCode\":\"VAV01\",\"skinCode\":\"\",\"version\":\"o1.114.1\",\"clientType\":3,\"playMode\":2,\"reconnect\":false}}");
                }
                else
                {
                    Send(
                        "{\"header\":{\"mId\":1,\"cId\":1,\"name\":\"Authenticate\",\"code\":0,\"dType\":1,\"dId\":0},\"body\":{\"username\":\"\",\"token\":\"" + userName + "\",\"languageCode\":\"RUS\",\"cageCode\":\"VAV01\",\"skinCode\":\"\",\"version\":\"o1.114.1\",\"clientType\":3,\"playMode\":1,\"reconnect\":false}}");
                }
            });
        }

        private void CloseBrowser()
        {
            if (_webDriver != null)
            {
                _webDriver.Close();
                _webDriver.Dispose();
                _webDriver = null;
            }
        }
        private void OpenBrowser()
        {
            CloseBrowser();
            if (_webDriver == null)
            {
                var chromeDriverService = ChromeDriverService.CreateDefaultService();
                chromeDriverService.HideCommandPromptWindow = true;
                var chromeOptions = new ChromeOptions();
                chromeOptions.AddArguments(new List<string>() { "no-sandbox", "headless", "disable-gpu" });
                _webDriver = new ChromeDriver(chromeDriverService, chromeOptions);
            }
        }
        private void OpenGame()
        {
            _cId = 2;
            _counter = 2;
            SetBet(_gambleBet);
            string token = "";
            // if (cbxAuthenticate.Checked)
            // {
            //     token = this._token;
            // }
            Send(
                "{\"header\":{\"mId\":" + _counter +",\"cId\": " + _cId +",\"name\":\"OpenGame\",\"code\":0,\"dType\":1,\"dId\":0},\"body\":{\"gameCode\":\"GAM_BOB\",\"token\":\"" + token + "\"}}");
        }

        private void Send(string data)
        {
            Log(data, true);
            _ws.Send(data);
        }
        private void button4_Click(object sender, EventArgs e)
        {
            btnSpin.Enabled = false;
            //{"header":{"mId":8,"cId":3,"name":"GameAction","code":0,"dType":2,"dId":14160873},"body":{"action":"SPIN","data":{"betAmount":800,"betPerLine":80,"paylines":10},"seqId":0}}	
            //{betAmount: 800, betPerLine: 80, paylines: 10}
            int betPerLine = 80;
            int paylines = 10;
            if (cbxAuthenticate.Checked)
            {
                _gambleBet = 800;
                betPerLine = 80;
            }
            else
            {
                _gambleBet = 10;
                betPerLine = 1;
            }
            _cId++;
            Log("USE NUMBER: " + _counter, true);
            Send("{\"header\":{\"mId\":" + _counter +
                    ",\"cId\":" + _cId + ",\"name\":\"GameAction\",\"code\":0,\"dType\":2,\"dId\":" + txtDId.Text + "},\"body\":{\"action\":\"SPIN\",\"data\":{\"betAmount\":" + _gambleBet + ",\"betPerLine\":" + betPerLine + ",\"paylines\":" + paylines + "},\"seqId\":0}}");
            _counter++;
            // Send("{\"header\":{\"mId\":" + counter +
            //              ",\"cId\":5,\"name\":\"GameEvent\",\"code\":1,\"dType\":2,\"dId\": " + dId +"}}");
            // counter++;
        }

        private void btnLadder_Click(object sender, EventArgs e)
        {
            btnLadder.Enabled = false;
            btnSpin.Enabled = false;

            Log("UES NUMBER: " + _counter, true);
            _cId++;
            Send("{\"header\":{\"mId\":" + _counter + ",\"cId\":" + _cId + ",\"name\":\"GameAction\",\"code\":0,\"dType\":2,\"dId\":" + txtDId.Text +"},\"body\":{\"action\":\"GAMBLE\",\"data\":{\"betAmount\":" + _gambleBet +"},\"seqId\":0,\"gameName\":\"GAMBLE_LADDER\"}}");
            _counter++;
        }

        private void btnLadderHalf_Click(object sender, EventArgs e)
        {
            if (_gambleBet >= 100)
            {
                _gambleBet /= 2;
                SetBet(_gambleBet);
                SetHalfBet(true);
            }
        }

        private void SetHalfBet(bool isHalfBet = false)
        {
            Invoke(new Action(() =>
            {
                lblHalfBet.Text = @"Half Bet: " + isHalfBet;
            }));
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            _ws.Close();
        }

        private IWebDriver _webDriver;
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                btnLogin.Enabled = false;
                OpenBrowser();
                _webDriver.Navigate().GoToUrl("https://vavada.com/en/");
                _webDriver.ReadCookies(_fileName);
                _webDriver.Navigate().Refresh();
                var userMoney =
                    _webDriver.FindElementBy(By.XPath("/html/body/div[1]/header/div/div[1]/div[2]/div[2]/span"));
                if (userMoney != null)
                {

                    MessageBox.Show(@"Login Successful");
                    _isLogin = true;
                    btnRefreshToken.Enabled = true;
                    return;
                }
                _webDriver.Navigate().GoToUrl("https://vavada.com/en/login");
                var userName = _webDriver.FindElementBy(By.XPath("//*[@id=\"_username\"]"));
                userName?.SendKeys(txtUserName.Text);
                var password = _webDriver.FindElementBy(By.XPath("//*[@id=\"_password\"]"));
                password?.SendKeys(txtPassword.Text);
                var rememberMe = _webDriver.FindElementBy(By.XPath("//*[@id=\"_remember_me\"]"));
                rememberMe?.SetAttribute("value", "1");
                var submit = _webDriver.FindElementBy(By.XPath("//*[@id=\"_submit\"]"));
                submit?.Click();
                userMoney =
                    _webDriver.FindElementBy(By.XPath("/html/body/div[1]/header/div/div[1]/div[2]/div[2]/span"));
                if (userMoney != null)
                {
                    MessageBox.Show(@"Login Successful");
                    _isLogin = true;
                    btnRefreshToken.Enabled = true;
                    _webDriver.SaveCookies(_fileName);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {
                btnLogin.Enabled = true;
                CloseBrowser();
            }
        }

        private void btnRefreshToken_Click(object sender, EventArgs e)
        {
            try
            {
                btnRefreshToken.Enabled = false;
                // https://vavada.com/en/games/books-and-bulls/play
                
                WebRequest webRequest = WebRequest.Create("https://vavada.com/en/games/books-and-bulls/play");
                webRequest.ReadCookies(_fileName);
                var response = webRequest.GetResponse();
                var responseStream = response.GetResponseStream();
                if (responseStream != null)
                {
                    var reader = new StreamReader(responseStream);
                    var receiveContent = reader.ReadToEnd();
                    reader.Close();
                    HtmlWeb web = new HtmlWeb();  
                    var htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(receiveContent);
                    var nodeList = htmlDoc.DocumentNode.SelectNodes("//iframe");
                    foreach (var node in nodeList)
                    {
                        var src = node.Attributes["src"];
                        var result = src.Value.Split(new string[] {"token="}, StringSplitOptions.None);
                        var token = result.Last();
                        InvokeIt(() =>
                        {
                            txtToken.Text = token;
                        });
                    }
                    return;
                }
                OpenBrowser();
                _webDriver.Navigate().GoToUrl("https://vavada.com/en/");
                _webDriver.ReadCookies(_fileName);
                _webDriver.Navigate().Refresh();
                _webDriver.Navigate().GoToUrl("https://vavada.com/en/games/books-and-bulls/play");
                var iframe = _webDriver.FindElementBy(By.TagName("iframe"));
                if (iframe != null)
                {
                    //src="https://play-rghr.igplatform.net/agg_plus_public/launch/wallets/VAVADA/games/GAM_BOB/open?languageCode=EN&amp;playMode=REAL&amp;token=fca0953b-50a7-4225-a0fe-9d585a794882"
                    var result = iframe.GetAttribute("src").Split(new string[] {"token="}, StringSplitOptions.None);
                    var token = result.Last();
                    InvokeIt(() =>
                    {
                        txtToken.Text = token;
                    });
                }

                _webDriver.Navigate().GoToUrl("https://vavada.com/en/");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {
                btnRefreshToken.Enabled = true;
                if (_webDriver != null)
                {
                    _webDriver.Close();
                    _webDriver.Dispose();
                    _webDriver = null;
                }
            }
        }

        private void InvokeIt(Action target)
        {
            Invoke(new Action(target));
        }
        private void btnReconnect_Click(object sender, EventArgs e)
        {
            btnClose.PerformClick();
            btnConnect.PerformClick();
        }
    }
}
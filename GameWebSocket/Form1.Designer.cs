﻿namespace GameWebSocket
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.Button();
            this.txtSend = new System.Windows.Forms.RichTextBox();
            this.btnSpin = new System.Windows.Forms.Button();
            this.btnLadder = new System.Windows.Forms.Button();
            this.lblBalance = new System.Windows.Forms.Label();
            this.lblBet = new System.Windows.Forms.Label();
            this.lblWinAmount = new System.Windows.Forms.Label();
            this.btnLadderHalf = new System.Windows.Forms.Button();
            this.txtRecieve = new System.Windows.Forms.RichTextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblHalfBet = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxAuthenticate = new System.Windows.Forms.CheckBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnRefreshToken = new System.Windows.Forms.Button();
            this.lblToken = new System.Windows.Forms.Label();
            this.btnReconnect = new System.Windows.Forms.Button();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.txtDId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(13, 503);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(4);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(237, 28);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Connect To Game Server";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSend
            // 
            this.txtSend.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSend.Location = new System.Drawing.Point(16, 15);
            this.txtSend.Margin = new System.Windows.Forms.Padding(4);
            this.txtSend.Name = "txtSend";
            this.txtSend.Size = new System.Drawing.Size(317, 293);
            this.txtSend.TabIndex = 1;
            this.txtSend.Text = "";
            // 
            // btnSpin
            // 
            this.btnSpin.Location = new System.Drawing.Point(13, 575);
            this.btnSpin.Margin = new System.Windows.Forms.Padding(4);
            this.btnSpin.Name = "btnSpin";
            this.btnSpin.Size = new System.Drawing.Size(480, 28);
            this.btnSpin.TabIndex = 4;
            this.btnSpin.Text = "Spin";
            this.btnSpin.UseVisualStyleBackColor = true;
            this.btnSpin.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnLadder
            // 
            this.btnLadder.Location = new System.Drawing.Point(13, 610);
            this.btnLadder.Name = "btnLadder";
            this.btnLadder.Size = new System.Drawing.Size(237, 34);
            this.btnLadder.TabIndex = 5;
            this.btnLadder.Text = "Ladder";
            this.btnLadder.UseVisualStyleBackColor = true;
            this.btnLadder.Click += new System.EventHandler(this.btnLadder_Click);
            // 
            // lblBalance
            // 
            this.lblBalance.Location = new System.Drawing.Point(496, 513);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(181, 26);
            this.lblBalance.TabIndex = 6;
            this.lblBalance.Text = "Balance: 0";
            // 
            // lblBet
            // 
            this.lblBet.Location = new System.Drawing.Point(496, 552);
            this.lblBet.Name = "lblBet";
            this.lblBet.Size = new System.Drawing.Size(181, 26);
            this.lblBet.TabIndex = 7;
            this.lblBet.Text = "Bet : 0";
            // 
            // lblWinAmount
            // 
            this.lblWinAmount.Location = new System.Drawing.Point(496, 587);
            this.lblWinAmount.Name = "lblWinAmount";
            this.lblWinAmount.Size = new System.Drawing.Size(181, 26);
            this.lblWinAmount.TabIndex = 8;
            this.lblWinAmount.Text = "Last Win Amount : 0";
            // 
            // btnLadderHalf
            // 
            this.btnLadderHalf.Location = new System.Drawing.Point(260, 610);
            this.btnLadderHalf.Name = "btnLadderHalf";
            this.btnLadderHalf.Size = new System.Drawing.Size(233, 34);
            this.btnLadderHalf.TabIndex = 9;
            this.btnLadderHalf.Text = " Ladder 1/2";
            this.btnLadderHalf.UseVisualStyleBackColor = true;
            this.btnLadderHalf.Click += new System.EventHandler(this.btnLadderHalf_Click);
            // 
            // txtRecieve
            // 
            this.txtRecieve.Location = new System.Drawing.Point(341, 13);
            this.txtRecieve.Margin = new System.Windows.Forms.Padding(4);
            this.txtRecieve.Name = "txtRecieve";
            this.txtRecieve.Size = new System.Drawing.Size(339, 293);
            this.txtRecieve.TabIndex = 10;
            this.txtRecieve.Text = "";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(13, 539);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(480, 28);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblHalfBet
            // 
            this.lblHalfBet.Location = new System.Drawing.Point(496, 623);
            this.lblHalfBet.Name = "lblHalfBet";
            this.lblHalfBet.Size = new System.Drawing.Size(181, 26);
            this.lblHalfBet.TabIndex = 12;
            this.lblHalfBet.Text = "Half Bet: False";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(17, 350);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(195, 22);
            this.txtUserName.TabIndex = 13;
            this.txtUserName.Text = "79013507066";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(260, 350);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(195, 22);
            this.txtPassword.TabIndex = 14;
            this.txtPassword.Text = "sonik4uiu";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(17, 321);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 26);
            this.label1.TabIndex = 15;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(260, 321);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 26);
            this.label2.TabIndex = 16;
            this.label2.Text = "Password";
            // 
            // cbxAuthenticate
            // 
            this.cbxAuthenticate.Location = new System.Drawing.Point(496, 399);
            this.cbxAuthenticate.Name = "cbxAuthenticate";
            this.cbxAuthenticate.Size = new System.Drawing.Size(179, 42);
            this.cbxAuthenticate.TabIndex = 17;
            this.cbxAuthenticate.Text = "Use Token";
            this.cbxAuthenticate.UseVisualStyleBackColor = true;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(17, 379);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(233, 28);
            this.btnLogin.TabIndex = 18;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnRefreshToken
            // 
            this.btnRefreshToken.Location = new System.Drawing.Point(258, 379);
            this.btnRefreshToken.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefreshToken.Name = "btnRefreshToken";
            this.btnRefreshToken.Size = new System.Drawing.Size(235, 28);
            this.btnRefreshToken.TabIndex = 19;
            this.btnRefreshToken.Text = "Refresh Token";
            this.btnRefreshToken.UseVisualStyleBackColor = true;
            this.btnRefreshToken.Click += new System.EventHandler(this.btnRefreshToken_Click);
            // 
            // lblToken
            // 
            this.lblToken.Location = new System.Drawing.Point(17, 411);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(233, 26);
            this.lblToken.TabIndex = 20;
            this.lblToken.Text = "Token: ";
            // 
            // btnReconnect
            // 
            this.btnReconnect.Location = new System.Drawing.Point(256, 503);
            this.btnReconnect.Margin = new System.Windows.Forms.Padding(4);
            this.btnReconnect.Name = "btnReconnect";
            this.btnReconnect.Size = new System.Drawing.Size(237, 28);
            this.btnReconnect.TabIndex = 21;
            this.btnReconnect.Text = "Reconnect";
            this.btnReconnect.UseVisualStyleBackColor = true;
            this.btnReconnect.Click += new System.EventHandler(this.btnReconnect_Click);
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(17, 440);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(233, 22);
            this.txtToken.TabIndex = 22;
            // 
            // txtDId
            // 
            this.txtDId.Location = new System.Drawing.Point(260, 440);
            this.txtDId.Name = "txtDId";
            this.txtDId.Size = new System.Drawing.Size(233, 22);
            this.txtDId.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(260, 411);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(233, 26);
            this.label3.TabIndex = 23;
            this.label3.Text = "DID";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 650);
            this.Controls.Add(this.txtDId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.btnReconnect);
            this.Controls.Add(this.lblToken);
            this.Controls.Add(this.btnRefreshToken);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.cbxAuthenticate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblHalfBet);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtRecieve);
            this.Controls.Add(this.btnLadderHalf);
            this.Controls.Add(this.lblWinAmount);
            this.Controls.Add(this.lblBet);
            this.Controls.Add(this.lblBalance);
            this.Controls.Add(this.btnLadder);
            this.Controls.Add(this.btnSpin);
            this.Controls.Add(this.txtSend);
            this.Controls.Add(this.btnConnect);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Books & Bull";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.TextBox txtDId;

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtToken;

        private System.Windows.Forms.Button btnReconnect;

        private System.Windows.Forms.Label lblToken;

        private System.Windows.Forms.Button btnRefreshToken;

        private System.Windows.Forms.Button btnLogin;

        private System.Windows.Forms.CheckBox cbxAuthenticate;

        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;

        private System.Windows.Forms.Label lblHalfBet;

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnSpin;

        private System.Windows.Forms.Button btnClose;

        private System.Windows.Forms.RichTextBox txtRecieve;
        private System.Windows.Forms.RichTextBox txtSend;

        private System.Windows.Forms.Button btnLadderHalf;

        private System.Windows.Forms.Label lblWinAmount;

        private System.Windows.Forms.Label lblBet;

        private System.Windows.Forms.Label lblBalance;

        private System.Windows.Forms.Button btnLadder;

        #endregion
    }
}

